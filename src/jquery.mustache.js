/*
 * mustache
 * https://github.com/gruntjs/grunt-init-jquery-sample
 *
 * Copyright (c) 2017 Eduardo Raupp
 * Licensed under the MIT license.
 */

(function($) {

  // Collection method.
  $.fn.mustache = function() {
    return this.each(function(i) {
      // Do something awesome to each selected element.
      $(this).html('awesome' + i);
    });
  };

  // Static method.
  $.mustache = function(options) {
    // Override default options with passed-in options.
    options = $.extend({}, $.mustache.options, options);
    // Return something awesome.
    return 'awesome' + options.punctuation;
  };

  // Static method default options.
  $.mustache.options = {
    punctuation: '.'
  };

  // Custom selector.
  $.expr[':'].mustache = function(elem) {
    // Is this element awesome?
    return $(elem).text().indexOf('awesome') !== -1;
  };

}(jQuery));

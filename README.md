# Mustache

Mustache

## Getting Started
Download the [production version][min] or the [development version][max].

[min]: https://raw.github.com/gruntjs/grunt-init-jquery-sample/master/dist/mustache.min.js
[max]: https://raw.github.com/gruntjs/grunt-init-jquery-sample/master/dist/mustache.js

In your web page:

```html
<script src="jquery.js"></script>
<script src="dist/mustache.min.js"></script>
<script>
jQuery(function($) {
  $.awesome(); // "awesome"
});
</script>
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Release History
_(Nothing yet)_
